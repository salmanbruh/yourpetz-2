from testimoni.forms import TestimoniForm
from django.test import TestCase

class FormsTestcase(TestCase):
    def test_form_has_correct_fields(self):
        form = TestimoniForm()
        self.assertIn('id_nama', form.as_p())
        self.assertIn('id_testimoni', form.as_p())
    
