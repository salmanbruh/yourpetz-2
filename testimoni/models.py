from django.db import models

# Create your models here.
class Testimoni(models.Model):
    nama = models.CharField(max_length=20, null=True)
    testimoni = models.TextField(max_length=1000, null=True)
    image = models.ImageField(upload_to='testimoni/',
                            default="testimoni/default.png",
                            blank=True, 
                            null=True,
                            width_field="width_field",
                            height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    
    def __str__(self):
        return '%s' % (self.nama)
