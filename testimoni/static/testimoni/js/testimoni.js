$(document).ready(function () {
    $(function(){
        $(':input[type="submit"]').prop('disabled', true);
        $(':input[type="file"]').prop('disabled', true);
        $('#testi-form').keyup(function () {
            if ($("#id_nama").val() == '' || $("#id_testimoni").val() == '') {
                $(':input[type="submit"]').prop('disabled', true);
            } else {
                $(':input[type="submit"]').prop('disabled', false);
            }
        });
    });
    $.ajax({
        async:true,
        type: "GET",
        url: "/testimoni_json/",
        dataType: "json",
        success: function (response) {
            let list_testi = response.testimonies;
            let banyakTesti = response.totalTestimoni;
            for (let index = 0; index < banyakTesti; index++){
                let testi = list_testi[index]

                $("#testimoni-list").prepend(
                    '<li class="media py-3">' + 
                        '<div class="picture mr-3">' +
                            '<img src="/media/'+testi.image+'" alt="img" class="img-fluid rounded-circle" id="user_img">' +
                        '</div>' +
                        '<div class="media-body">' +
                            '<h5 class="mt-0 mb-1"> '+ 
                                '<b>' + testi.nama + '</b>' +
                            '</h5>' +
                            testi.testimoni +
                        '</div>' +
                    '</li>' +
                    '<hr class="my-3 rounded-circle" style="border:1px solid grey"></hr>'
                )
            }
        },
        error : function(){
            alert("Internal error occurred. Please reload the page.")
        },
        type : 'GET',
    });
});

$("#form-show").click(function (e) { 
    e.preventDefault();
    $("#testi-form").toggle("slow");
});


$('#testi-form').on('submit',function(e){
    e.preventDefault()
    console.log($("#id_nama").val())
    var namainput = $("#id_nama").val();
    var testiinput = $("#id_testimoni").val();
    var imageinput = $("#id_image").val();
    $.ajax({
        type:"POST",
        url:"/testimoni_beta/testimoni_created/",
        data:{
            'nama':namainput,
            'testimoni':testiinput,
            'image':imageinput,
            'csrfmiddlewaretoken':$("input[name=csrfmiddlewaretoken]").val(),
        },
        success:function(result){
            console.log("Success")
            
            $("#testimoni-list").prepend(
                '<li class="media py-3">' + 
                    '<div class="picture mr-3">' +
                        '<img src="/media/' + result.image + '" alt="img" class="img-fluid rounded-circle" id="user_img">' +
                    '</div>' +
                    '<div class="media-body">' +
                        '<h5 class="mt-0 mb-1"> '+
                            '<b>' + result.nama + '</b>' +
                        '</h5>' +
                        result.testimoni +
                    '</div>' +
                '</li>' +
                '<hr class="my-3 rounded-circle" style="border:1px solid grey"></hr>'
            )
            alert("Works")
            $("#id_nama").val('')
            $("#id_testimoni").val('')
            $("#id_image").val('')
        },
        error : function() {
            alert("Internal error occurred. Please reload the page.")
        }
    });
});




$(function() {
    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});