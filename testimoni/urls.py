from django.urls import path
from testimoni.views import list_testimoni,form_testimoni,single_page_testimoni,testimoni_json, add_testi
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('testimoni/', list_testimoni, name='list_testimoni'),
    path('testimoni_form/', form_testimoni, name='form_testimoni'),
    path('testimoni_beta/', single_page_testimoni, name='testimoni'),
    path('testimoni_beta/testimoni_created/', add_testi, name='testimoni_created'),
    path('testimoni_json/', testimoni_json, name='json'),
] 

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)