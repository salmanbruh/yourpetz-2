from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import json
from testimoni.models import Testimoni
from testimoni.forms import TestimoniForm
from django.conf import settings
from django.forms.models import model_to_dict
from django.views import View

# Create your views here.
def list_testimoni(request):
    testies = Testimoni.objects.all()
    context = {
        'testi':testies,
        'header':'Pendapat pengguna situs kami ...'
    }

    return render(request, 'testimoni/list-testimoni.html', context)

def form_testimoni(request):
    if request.method == "POST":
        form = TestimoniForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/testimoni/')
    else:
        form = TestimoniForm()

    context = {
        'page_title':'Form Testimoni',
        'form':form,
        'header':'Pendapat anda ...'
    }
    return render(request, "testimoni/form-testimoni.html", context)

def single_page_testimoni(request):
    form = TestimoniForm(request.POST or None, request.FILES or None)
        
    context = {
            'page_title':'Testimoni',
            'form':form,
            'header':'Pendapat anda ...',
        }
    
    return render(request, 'testimoni/testimoni.html', context)

def add_testi(request):
    print(request.FILES)
    nama = request.POST.get('nama')
    testi = request.POST.get('testimoni')
    image = request.POST['image']
    if image == "":
        image = "testimoni/default.png"
    else:
        image = "testimoni/" + image[12:]
    testimoni = Testimoni(nama=nama, testimoni=testi, image=image)
    testimoni.save()
    response_data = {}
    response_data['nama'] = nama
    response_data['testimoni'] = testi
    response_data['image'] = image
    response_data['success'] = "Berhasil menambahkan testimoni!"
    return JsonResponse(response_data)

def testimoni_json(request):
    data = list(Testimoni.objects.values('nama', 'testimoni', 'image'))
    return JsonResponse({'totalTestimoni':len(data) ,'testimonies': data})