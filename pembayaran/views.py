from django.shortcuts import render, redirect
from .forms import alamat_pengiriman_form, ongkos_kirim_form
from .models import alamat_pengiriman, ongkos_kirim
from landing.models import Product
# Create your views here.

def pembayaran_view(request):
    if(not request.user.is_authenticated):
        return redirect('/login/')
    id = request.GET.get('id')
    arg = Product.objects.get(pk=int(id))
    if 'context' in request.session:
        return biaya_view(request, request.session['context'])
    form = alamat_pengiriman_form(request.POST or None)
    if(form.is_valid()):
        count = 0
        context = {}
        for i in form:
            if count == 0:
                context['nama'] = i.value()
            elif count == 1:
                context['nomor_hp'] = i.value()
            elif count == 2:
                context['provinsi'] = i.value()
            elif count == 3:
                context['kota'] = i.value()
            elif count == 4:
                context['kecamatan'] = i.value()
            elif count == 5:
                context['pos'] = i.value()
            count += 1
        temp = ongkos_kirim()
        context['ongkos_kirim'] = temp.biaya
        request.session['context'] = context
        context['harga_barang'] = arg.price
        return biaya_view(request, context)
        

    context = {
        'alamat' : form,
        'page_title' : "Detail Belanja",
        'barang' : arg,
    }
    return render(request, "pembayaran/pembayaran.html", context)


def biaya_view(request, newContext = {}):
    id = request.GET.get('id')
    arg = Product.objects.get(pk=int(id))
    form = ongkos_kirim_form(request.POST or None)
    if(form.is_valid() and 'ada' in request.session):
        context = {
            'ongkos_kirim' : newContext['ongkos_kirim'],
            'harga_barang' : newContext['harga_barang'],
            'page_title' : 'Detail Belanja',
        }
        return bayar_view(request, context)
    request.session['ada'] = True
    context = {
        'barang' : arg,
        'nama' : newContext['nama'],
        'kota' : newContext['kota'],
        'nomor_hp' : newContext['nomor_hp'],
        'provinsi' : newContext['provinsi'],
        'kecamatan' : newContext['kecamatan'],
        'pos' : newContext['pos'],
        'ongkos_kirim' : newContext['ongkos_kirim'],
        'harga_barang' : newContext['harga_barang'],
        'page_title' : 'Ringkasan Belanja',
    }
    return render(request, "pembayaran/biaya.html", context)

def bayar_view(request, newContext = {}):
    context = {
        'harga_barang' : newContext['harga_barang'],
        'ongkos_kirim' : newContext['ongkos_kirim'],
        'page_title' : 'Metode Pembayaran',
    }
    return render(request, 'pembayaran/bayar.html', context)

def success(request):
    request.session.clear()
    context = {
        'page_title' : 'Transaksi Berhasil',
    }
    return render(request, 'pembayaran/success.html', context)
