from django.db import models
from random import random, randrange
# Create your models here.

class alamat_pengiriman(models.Model):
    nama_penerima = models.CharField(max_length=50)
    nomor_hp = models.CharField(max_length=50)
    provinsi = models.CharField(max_length=50)
    kota = models.CharField(max_length=50)
    kecamatan = models.CharField(max_length=50)
    kode_pos = models.IntegerField()

class ongkos_kirim(models.Model):
    biaya = models.IntegerField()
    total = models.IntegerField(default=0)
    biaya.default = randrange(10, 50) * 1000
