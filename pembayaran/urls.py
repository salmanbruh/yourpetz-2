from django.urls import path, include
from . import views
# from pembayaran.views import <namamethod>

urlpatterns = [
    path('pembayaran/', views.pembayaran_view),
    path('pembayaran/success/', views.success),
]