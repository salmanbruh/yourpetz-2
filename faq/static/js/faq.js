$(document).ready(function () {
    $.ajax({
        async:true,
        type: "GET",
        url: "/faqs/json/",
        dataType: "json",
        success: function (response) {
            let list_faq = response.faqs;
            let banyakFaq = response.semuaFaq;
            for (let index = 0; index < banyakFaq; index++){
                let faq = list_faq[index]

                $("#listOfFaqs").append(
                    '<li class="media py-3">' + 
                        '<div class="flex-row">' +
                            '<div class= "col-sm-12">' +
                                '<div class="line"></div>' +
                                '<h3 class=titlePertanyaan>'+faq.pertanyaan+'</h3>' +
                                '<p class=jawabanPertanyaan>'+faq.jawaban+ '</p>'+
                            '</div>'+
                        '</div>'+
                    '</li>'
                )
            }
        },
        error : function(){
            alert("Internal error occurred. Please reload the page.")
        },
        type : 'GET',
    });
});
