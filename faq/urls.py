from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'faq'

urlpatterns = [
    path('', views.faq_list, name='faq_list'),
    path('new/', views.new_faq, name='new_faq'),
    path('json/', views.faq_json, name='json'),
]
