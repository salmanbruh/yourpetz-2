from django.shortcuts import render
from django.views import generic
from .models import Faq
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .forms import FaqForms
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView

def faq_list(request):
    if request.method == 'GET':
        faqs = Faq.objects.order_by('pertanyaan')
        return render(request, 'faq_list.html', {'faqs':faqs})
    

def new_faq(request):
    if request.method == "POST":
        form = FaqForms(request.POST or None)
        if form.is_valid():
            faq = form.save()
            faq.save()
            return redirect('/faqs/')
    else:
        form = FaqForms()

    context = {
        'page_title':'Form FAQs',
        'form':form,
    }

    return render(request, 'new_faq.html', context)

def faq_json(request):
    data = list(Faq.objects.values('pertanyaan', 'jawaban'))
    return JsonResponse({'semuaFaq':len(data), 'faqs': data})