# **PPW - Tugas Kelompok 2**
[![pipeline status](https://gitlab.com/Salman-Ahmad-N/yourpetz-2/badges/master/pipeline.svg)](https://gitlab.com/Salman-Ahmad-N/yourpetz-2/commits/master)

Repository of the Second Group Task for Web Design & Programming

## **Anggota Kelompok KB09**
- Ahmad Haulian Yoga Pratama
- Muhammad Rian Alpajirin
- Muhammad Rasyid
- Salman Ahmad Nurhoiriza

## **About our Application**
Aplikasi ini berbasis jual barang dari sebuah usaha fiktif yang kami buat bernama 
'YourPets'. Aplikasi ini menjual barang-barang yang berkaitan dengan hewan 
peliharaan seperti makanan hewan, tempat tinggal hewan, dan lain-lain.
Proses penjualannya yaitu user memilih barang yang ingin di-order, kemudian
checkout dengan mengisi alamat tujuan dan metode pembayaran. Terdapat dua macam
metode pembayaran yang dapat dilakukan oleh user, yaitu dengan transfer lewat
ATM dan bayar melalui GOPay atau OVO.

> [HerokuApp Link](https://your-petz.herokuapp.com)

## **Application Features**
- Menampilkan list barang
- Menambahkan barang untuk dijual
- Mengkategorisasi barang
- Membeli barang
- Kotak testimoni untuk pembeli
- FAQ
- About