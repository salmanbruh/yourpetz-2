from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_product, see_desc
from .models import Product, Profile
from .forms import MakeProduct

class TestLanding(TestCase):
    


    def test_url_is_exist(self):
        self.response_landing = Client().get("")
        self.assertEqual(self.response_landing.status_code, 200)
        self.response_add_product = Client().get("/addproduct/")
        self.assertEqual(self.response_add_product.status_code, 302)
    
    
    def test_url_using_function(self):
        found_landing = resolve("/")
        self.assertEqual(found_landing.func, index)
        found_add_product = resolve("/addproduct/")
        self.assertEqual(found_add_product.func, add_product)

    def test_model_can_create_product(self):
        Product.objects.create(
            name = "Kucing",
            price =  200000,
            category = "Pets",
            description = "Hmmm.."
        )

        count = Product.objects.all().count()
        self.assertEqual(count, 1)
        response = Client().get("")
        self.assertContains(response, "Kucing")

        found = resolve("/desc/1/")
        self.assertEqual(found.func, see_desc)

        self.response_landing = Client().get("/desc/1/")
        self.assertEqual(self.response_landing.status_code, 200)

    def test_form_invalid(self):
        form = MakeProduct(data={
            'name' : "Kucing",
            'imgProduct' : "No image uploaded (this will cause an error)",
            'price' :  2000000,
            'category' : "Pets",
            'description' : "Hmmm..",
        })

        self.assertFalse(form.is_valid())
