from django import forms
from .models import Product
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class MakeProduct(forms.ModelForm):
    
    name = forms.CharField(
        label = "name", widget = forms.TextInput(attrs={
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default"
        }),

    )

    imgProduct = forms.ImageField(
        label = "imgProduct",
        widget = forms.FileInput(attrs={
            "class" : "form-control",
            "type" : "file",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",            
        })
    )

    category = forms.ChoiceField(
        label = "category", 
        
        choices = [
            ("Pets", "Pets"),
            ("Toys", "Toys"),
            ("Foods", "Foods"),
            ("Others", "Others"),
            ],

        widget = forms.Select(attrs = {
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",
        })
    )

    price = forms.IntegerField(
        label = "price",
        widget = forms.NumberInput(attrs={
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",

        })
    )

    description = forms.CharField(
        label = "description", widget = forms.Textarea(attrs={
            "class" : "form-control",
            "type" : "text",
            "aria-label" : "Default",
            "aria-describedby" : "inputGroup-sizing-default",
        })
    )
   
    class Meta:
        model = Product
        fields = ["name", "price", "imgProduct" , "category", "description"]


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(
        widget = forms.TextInput(attrs={
            "placeholder" : "First Name",
            "id" : "django_form",

        }), 
        max_length=100)
    last_name = forms.CharField(
        widget = forms.TextInput(attrs={
            "placeholder" : "Last Name",
            "id" : "django_form",

        }),
        max_length=100)
    email = forms.EmailField(
        widget = forms.TextInput(attrs={
            "placeholder" : "Email",
            "id" : "django_form",

        }),
        max_length=150)
    
    username = forms.CharField(
        widget = forms.TextInput(attrs={
            "placeholder" : "Username",
            "id" : "django_form",

        }), 
        max_length=20)

    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={
            "placeholder" : "Password",
            "id" : "django_form",
        }),
        max_length=20,
    )

    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={
            "placeholder" : "Confirmation Password",
            "id" : "django_form",
        }),
        max_length=20,
    )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')