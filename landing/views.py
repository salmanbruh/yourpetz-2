from django.shortcuts import render, redirect
from .models import Product
from .forms import MakeProduct
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.decorators import login_required
from .forms import SignUpForm
from django.http import JsonResponse, HttpResponse
from django.core import serializers


def see_desc(request, id):
    arg = Product.objects.get(pk=id)
    if request.method == 'POST':
        return redirect('/pembayaran/?id=' + str(id))
    context = {
        "barang" : arg,
        "page_title" : arg.name
    }
    return render(request, "landing/desc.html", context)

def index(request):


    all_product = Product.objects.all()

    arguments = {
        "page_title" : "Welcome to Your Pets",
        "products" : all_product,
    }
    return render(request, "landing/landing_page.html", arguments)

def newest_product(request):
    products = Product.objects.all().order_by("-id")[:3:]
    post_products = serializers.serialize('json', products)
    return HttpResponse(post_products, content_type="text/json-comment-filtered")

def add_product(request):
    if not request.user.is_authenticated:
        return redirect("landing:index")

    a_form = MakeProduct(request.POST or None, request.FILES or None)
    
    arguments = {
        "page_title" : "Add a Product",
        "form" : a_form,
    }

    if request.method == "POST" :
        if a_form.is_valid():
            a_form.save()
            return redirect("landing:add_my_product")
        else:
            a_form = MakeProduct()

    return render(request,"landing/addproduct.html", arguments)


def login_page(request):
    if request.user.is_authenticated:
        return redirect("landing:index")
    
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            request.session["user"] = request.POST["username"]
            return redirect("landing:index")
    
    form = AuthenticationForm()
    return render(request, "landing/login.html", {"form": form, "page_title" : "Login to Your Account"})

def signup_page(request):
    if request.user.is_authenticated:
        return redirect("landing:index")
    
    if request.method == "POST":
        form = SignUpForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()

            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.email = form.cleaned_data.get('email')
            user.save()

            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            request.session["user"] = request.POST["username"]
            return redirect("landing:index")
        else:

            form = SignUpForm()
    form = SignUpForm()
    return render(request, "landing/signup.html", {"form" : form, "page_title" : "Sign Up to Your Pets"})


def logout_account(request):
    logout(request)
    return redirect("landing:index")
