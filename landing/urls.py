from django.urls import path, include
from landing.views import index
from landing.views import add_product
from landing.views import see_desc
from django.conf import settings
from django.conf.urls.static import static
from landing.views import logout_account
from landing.views import login_page
from landing.views import signup_page
from landing.views import newest_product

app_name = "landing"

urlpatterns = [
    path("", index, name='index'),
    path("addproduct/", add_product, name='add_my_product'),
    path("desc", index, name="description2"),
    path("desc/<int:id>/", see_desc, name="description"),
    path("login/", login_page, name="login_page"),
    path("signup/", signup_page, name="signup_page"),
    path("logout/", logout_account, name="logout_account"),
    path("ajax_call_products", newest_product, name="ajax_call"),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)